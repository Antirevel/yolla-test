Symfony 3.1 + docker lamp.
Requires docker and docker-compose.

**RUN**:
docker-compose up -d

**STOP**:
docker-compose stop

**CONTAINERS LIST**:
docker-compose ps

To run composer and symfony commands use php container.
NOTE: use www-data user instead of default (root)

Run single command:
docker exec -u www-data -it CONTAINER_NAME COMMAND

Connect to terminal:
docker exec -u www-data -it CONTAINER_NAME /bin/bash