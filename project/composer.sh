#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ACTION="install"

# read parameters
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -u|--update)
    ACTION='update'
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

# run composer command
runuser -u www-data composer ${ACTION}